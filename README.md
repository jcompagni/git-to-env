# git-to-env

A Leiningen plugin to do many wonderful things.

## Usage



Put `[git-to-env "0.1.0-SNAPSHOT"]` into the `:plugins` vector of your project.clj.

git to env is a middleware the runs automatically on pproject build. 

Set `:path` in the `:git-to-env` hash to determine where in the project file the git ref is stored.
Ex.
```:git-to-env {:path [:profiles :uberjar :env :git-ref]}```

## License

Copyright © 2019 

Distributed under the Eclipse Public License either version 1.0.

