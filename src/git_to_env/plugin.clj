(ns git-to-env.plugin
  (:require
   clojure.string
   [clojure.java.shell :refer [sh]]))

(defn most-recent-ref []
  (->> (apply sh ["git" "rev-parse" "HEAD"]) :out clojure.string/trim))

(defn middleware
  "Set most recent git ref as a value in project hash"
  [{{:keys [path]} :git-to-env :as project}]
  (let [ref (most-recent-ref)]
    (println "git ref:" ref)
    (assoc-in project path ref) ))

